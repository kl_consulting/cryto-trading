## Introduction

GIT is a versionning system

The versioning consist of tracing changes in the code

- WHEN did the change in the code happened ?
- WHAT have changed in the code ?
- WHO did the change in the code ?


The code is stored in a REPOSITORY 

And sometimes we will call it a repo ! ^_^

## How it works

- You will have a local copy of a remote status/snapshot/image of the code.
- You will work on it, COMMIT changes localy...
- When you need to publish / backup something you will PUSH your changes to the remote repository.

## What is a branch

Branch are copies of the code you can work on... 

It'an organisation feathur, it's your/our choice how we organize.

In this project we will have 2 branches :

- master : the production code
- devel : the code that is in developpement status

## Git command line : all you need to know !

- <b>git clone</b> => clone a repository locally, on your device
- <b>git checkout</b> => change branch in the repository
- <b>git status</b> => compare your code to the remote branch
- <b>git branch</b> => check witch branch you have, and are working on
- <b>git pull</b> => get changes from other developpers insite your local copy of the repo.
- <b>git add</b> : add files & directories to the repository
- <b>git commit -m "my comment" </b>=> save changes on the code in you local repository
- <b>git push </b>=> send your local changes to the code to the repository

So now what you have to do is :
- clone the repo / get devel branch / code something / commit / push 


